.. dev documentation master file, created by
   sphinx-quickstart on Wed Dec  5 22:06:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dev's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Contents:
.. toctree::
   :maxdepth: 2

   modules/views
