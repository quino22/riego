from django.urls import path, include
from django.conf.urls import url
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login, logout_then_login
from . import views, servicios

from riegoApp.views import index, ChartView

app_name = 'riegoApp'

urlpatterns = [
    url(r'^index/$',  login_required(index), name='index'),
    url(r'^accounts/login',login,{'template_name':'login.html'}, name="login"),
    url(r'^logout/',logout_then_login,name="logout"),
    url(r'^chart/',ChartView.as_view(),name="chart"),
    url(r'^reporte/',servicios.reportes,name="report"),
    url(r'^ambientes/',servicios.LlenarAmbientes,name="ambientes"),
    url(r'^sensores/',servicios.LlenarSensores,name="sensores"),
    path('guardarDatos/',servicios.guardarDatosSensor,name='guardar-datos'),
    
]