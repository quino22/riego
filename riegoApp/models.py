from django.contrib.auth.models import User
from django.db import models
#from django.contrib.gis.db import models
# Create your models here.


class Distritos(models.Model):
    """
    Stores a single blog entry, related to :model:`riegoApp.Blog` and
    :model:`auth.User`.
    """
    nombre = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'Distrito'
        verbose_name_plural = "Distritos"

    def __str__(self):
        return self.nombre
    
class Sectores(models.Model):
    """
    Guarda una relacion con :model:`riegoApp.Distritos` y 
    el nombre del distrito
    """
    distrito = models.ForeignKey(Distritos, related_name='distrito', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'Sector'
        verbose_name_plural = "Sectores"

    def __str__(self):
        return  self.distrito.nombre + " - " + self.nombre

class Ambiente(models.Model):
    """
    Guarda una relacion con :model:`riegoApp.Sectores` y 
    una descripcion del ambiente y la densidad de plantas que existe
    """
    sector = models.ForeignKey(Sectores, related_name='sectores', on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=50)
    densidad = models.FloatField(default = 0)

    class Meta:
        verbose_name = 'Ambiente'
        verbose_name_plural = "Ambientes"
    

class Sensor(models.Model):
    """
    Guarda una relacion con :model:`riegoApp.Ambiente`,
    la fecha de cuando a sido instalado, dos estados( activo, inactivo),
    y la latitud y longitud, para saber donde esta ubicado
    """
    ambiente = models.ForeignKey(Ambiente, related_name='sensor', on_delete=models.CASCADE)
    fecha = models.DateTimeField(auto_now=True)
    ACTIVO = 1
    INACTIVO = 2
    ESTADOS = (
        (ACTIVO, 'Activo'),
        (INACTIVO, 'Inactivo'),
    )
    estado = models.IntegerField(choices=ESTADOS, default=ACTIVO)
    latitud = models.FloatField(default=0)
    longitud = models.FloatField(default=0)
    #mpoly = models.MultiPolygonField()

    class Meta:
        verbose_name = 'Sensor'
        verbose_name_plural = "Sensores"
    

class Detalle_Sensor(models.Model):
    sensor = models.ForeignKey(Sensor ,related_name='sensor_detalle', on_delete=models.CASCADE)
    humedad = models.IntegerField(default=0)
    fecha = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Detalle del sensor'
        verbose_name_plural = "Detalle de los sensores"
    

class Riego(models.Model):
    fechaInicio = models.DateTimeField(auto_now_add=True)
    fechaFin = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Riego'
        verbose_name_plural = "Riego"

class Arduino(User):
    sensor = models.ForeignKey(Sensor, related_name='sensor', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Arduino'
        verbose_name_plural = "Arduino"

class Reportes(models.Model):
    #arduino = models.ForeignKey(Arduino, related_name='arduino', on_delete=models.CASCADE)
    fecha = models.DateField(auto_now_add=True)
    #riego = models.ForeignKey(Riego, related_name='riego', on_delete=models.CASCADE)
    reporte = models.FileField(upload_to='reportes', blank=True, null=True, default="")

    class Meta:
        verbose_name = 'Reporte'
        verbose_name_plural = "Reportes"
