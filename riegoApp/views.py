from django.shortcuts import render
from django.template.loader import get_template
from django.http import HttpResponse
from django.views.generic import View
# Create your views here.
from riegoApp.models import *
from django.db.models.functions import Extract
from django.db.models import Count, Sum
from django.db.models.functions import TruncYear, TruncMonth



def index(request):
    t = get_template('index.html')
    d = Distritos.objects.get(pk = 1)
    html = t.render({'distrito':d}, request)

    return HttpResponse(html)


class ChartView(View):
    def get(self, request, *args, **kwargs):
        t = get_template('chart.html')
        filtro = self.request.GET['filtro']
        if filtro == '0':
            tipoFecha = self.request.GET['tipo']
            if tipoFecha == '0': #ANIO
                detalle = Detalle_Sensor.objects \
                    .annotate(date=TruncYear('fecha'))\
                    .values('date')\
                    .annotate(humedad=Sum('humedad'),c=Count('id'))\
                    .values('date', 'humedad','c')
                
            elif tipoFecha == '1':
                detalle = Detalle_Sensor.objects \
                    .annotate(date=TruncMonth('fecha'))\
                    .values('date')\
                    .annotate(humedad=Sum('humedad'),c=Count('id'))\
                    .values('date', 'humedad','c').filter(fecha__year='2018')
            
            contexto = {"detalle": detalle,"tipo":tipoFecha}

        else:
            ambiente = self.request.GET['ambiente']
            sensor = self.request.GET['sensor']
            if (sensor == '-1'):
                ambiente = Ambiente.objects.get(pk = ambiente)
                sensores = ambiente.sensor.all()
            
                detalle = Detalle_Sensor.objects \
                    .annotate(date=TruncMonth('fecha'))\
                    .values('date')\
                    .annotate(humedad=Sum('humedad'),c=Count('id'))\
                    .values('date', 'humedad','c').filter(fecha__year='2018',sensor__in = sensores).order_by('fecha')
            else:

                detalle = Detalle_Sensor.objects \
                    .annotate(date=TruncMonth('fecha'))\
                    .values('date')\
                    .annotate(humedad=Sum('humedad'),c=Count('id'))\
                    .values('date', 'humedad','c').filter(fecha__year='2018',sensor = sensor).order_by('fecha')



            contexto = {"detalle": detalle}
        
        html = t.render(contexto)
        return HttpResponse(html)





    