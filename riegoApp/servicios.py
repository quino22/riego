import pdfkit
import json
import io
import datetime
from django.shortcuts import render
from django.template.loader import get_template
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt
from .models import Sensor, Detalle_Sensor, Distritos, Ambiente, Reportes
from django.db.models import Count, Sum
from django.db.models.functions import TruncYear, TruncMonth
from django.core.files import File
# Create your views here.


@csrf_exempt
def guardarDatosSensor(request):
    """
    Guarda el porcentaje de humendad de un sensor en la tabla Detalle_Sensor
    """
    sensor = request.GET.get('idSensor', '')
    dataSensor = float(request.GET.get('dataSensor', ''))
    #d = Distritos(nombre = "NOMRBE")
    #d.save()
    sensor = Sensor.objects.get(pk = sensor)
    detalleSensor = Detalle_Sensor(sensor = sensor,humedad = dataSensor)
    detalleSensor.save()

    return HttpResponse("OKA")


def reportes(request):
    """
    Genera reportes de un año espesifico
    """
    print("ewqe")
    t = get_template('report.html')
    print(request.GET)
    filtro = request.GET['filtro']
    if filtro == '0':
        tipoFecha = request.GET['tipo']
        if tipoFecha == '0': #ANIO
            detalle = Detalle_Sensor.objects \
                .annotate(date=TruncYear('fecha'))\
                .values('date')\
                .annotate(humedad=Sum('humedad'),c=Count('id'))\
                .values('date', 'humedad','c')
            
        elif tipoFecha == '1':
            detalle = Detalle_Sensor.objects \
                .annotate(date=TruncMonth('fecha'))\
                .values('date')\
                .annotate(humedad=Sum('humedad'),c=Count('id'))\
                .values('date', 'humedad','c').filter(fecha__year='2018')
        
        contexto = {"detalle": detalle,"tipo":tipoFecha}

    else:
        ambiente = request.GET['ambiente']
        sensor = request.GET['sensor']
        if (sensor == '-1'):
            ambiente = Ambiente.objects.get(pk = ambiente)
            sensores = ambiente.sensor.all()
        
            detalle = Detalle_Sensor.objects \
                .annotate(date=TruncMonth('fecha'))\
                .values('date')\
                .annotate(humedad=Sum('humedad'),c=Count('id'))\
                .values('date', 'humedad','c').filter(fecha__year='2018',sensor__in = sensores).order_by('fecha')
        else:

            detalle = Detalle_Sensor.objects \
                .annotate(date=TruncMonth('fecha'))\
                .values('date')\
                .annotate(humedad=Sum('humedad'),c=Count('id'))\
                .values('date', 'humedad','c').filter(fecha__year='2018',sensor = sensor).order_by('fecha')



        contexto = {"detalle": detalle}

    now = datetime.datetime.now()
    contexto['fecha'] = now
    html = t.render(contexto)
    #return HttpResponse(html)
    pdfkit.from_string(html,"media/nuevo3.pdf")

    fs = FileSystemStorage()
    filename = "nuevo3.pdf"
    if fs.exists(filename):
        with fs.open(filename) as pdf:
            reopen = open(pdf.name, 'rb')
            django_file = File(reopen)
            r = Reportes.objects.all().count()
            rIns = Reportes()
            name = 'reporte_'+str(r)+'.pdf'
            rIns.reporte.save(name,django_file,save=True)
            # response = HttpResponse(pdf, content_type='application/pdf')
            # response['Content-Disposition'] = 'attachment; filename="reporte.pdf"'
            return HttpResponse(rIns.reporte.url)
    

def LlenarAmbientes(request):
    """
    Obtiene todos los ambientes y los envia al frontend
    """
    js = []
    ambientes = Ambiente.objects.all()
    for a in ambientes:
        js.append({
            'pk':a.pk,
            'desc':a.descripcion,
        })
    
    return HttpResponse(json.dumps(js), content_type="application/json")


def LlenarSensores(request):
    """
    Obtiene todos los sensoser de un ambientes y los envia al frontend
    """
    ambiente = request.GET['ambiente']
    js = []
    
    sensores = Sensor.objects.filter(ambiente = ambiente)

    cont = 1
    for s in sensores:
        js.append({
            'pk':s.pk,
            'desc':'Sensor '+str(cont)
        })
        cont += 1
    
    return HttpResponse(json.dumps(js), content_type="application/json")