# Generated by Django 2.0.2 on 2018-11-29 04:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('riegoApp', '0004_auto_20181121_1218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='detalle_sensor',
            name='humedad',
            field=models.IntegerField(default=0),
        ),
    ]
