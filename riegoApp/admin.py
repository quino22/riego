from django.contrib import admin

from .models import *
# Register your models here.

admin.site.register(Distritos)
admin.site.register(Sectores)
admin.site.register(Ambiente)
admin.site.register(Sensor)
admin.site.register(Detalle_Sensor)
admin.site.register(Riego)
admin.site.register(Arduino)
admin.site.register(Reportes)
